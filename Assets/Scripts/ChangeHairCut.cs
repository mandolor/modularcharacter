using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEditor;

public class ChangeHairCut : MonoBehaviour
{
    GameObject friendObj;

    SkinnedMeshRenderer mainSkinnMeshHairCut;
    SkinnedMeshRenderer otherSkinnMeshHairCut;

    SkinnedMeshRenderer mainSkinnMeshBottom;
    SkinnedMeshRenderer otherSkinnMeshBottom;

    SkinnedMeshRenderer mainSkinnMeshTop;
    SkinnedMeshRenderer otherSkinnmeshTop;

    Mesh newMeshHairCut;
    Mesh oldMeshHairCut;

    Mesh newMeshBottom;
    Mesh oldMeshBottom;

    Mesh newMeshTop;
    Mesh oldMeshTop;

    public string modelName = "";

    // Start is called before the first frame update
    void Start()
    {
        friendObj = GameObject.Find(modelName).gameObject;
        Debug.Log(modelName);

        if (!friendObj)
            Debug.Log("friednObject is null");

        mainSkinnMeshHairCut = gameObject.transform.Find("Wolf3D_Hair").GetComponent<SkinnedMeshRenderer>();
        otherSkinnMeshHairCut = friendObj.transform.Find("Wolf3D_Hair").gameObject.GetComponent<SkinnedMeshRenderer>();
        newMeshHairCut = otherSkinnMeshHairCut.sharedMesh;
        oldMeshHairCut = mainSkinnMeshHairCut.sharedMesh;

        if (!newMeshHairCut)
            Debug.Log("newMeshHairCut is null");

        if (!oldMeshHairCut)
            Debug.Log("oldMeshHaircut is null");

        mainSkinnMeshTop = gameObject.transform.Find("Wolf3D_Outfit_Top").GetComponent<SkinnedMeshRenderer>();
        otherSkinnmeshTop = friendObj.transform.Find("Wolf3D_Outfit_Top").gameObject.GetComponent<SkinnedMeshRenderer>();
        newMeshTop = otherSkinnmeshTop.sharedMesh;
        oldMeshTop = mainSkinnMeshTop.sharedMesh;

        if (!newMeshTop)
            Debug.Log("newMeshTop is null");

        if (!oldMeshTop)
            Debug.Log("oldMeshTop is null");

        mainSkinnMeshBottom = gameObject.transform.Find("Wolf3D_Outfit_Bottom").GetComponent<SkinnedMeshRenderer>();
        otherSkinnMeshBottom = friendObj.transform.Find("Wolf3D_Outfit_Bottom").gameObject.GetComponent<SkinnedMeshRenderer>();
        newMeshBottom = otherSkinnMeshBottom.sharedMesh;
        oldMeshBottom = mainSkinnMeshBottom.sharedMesh;

        if (!newMeshBottom)
            Debug.Log("newMeshBottom is null");

        if (!oldMeshBottom)
            Debug.Log("oldMeshBottom is null");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            oldMeshHairCut = mainSkinnMeshHairCut.sharedMesh;
            mainSkinnMeshHairCut.sharedMesh = newMeshHairCut;
            newMeshHairCut = oldMeshHairCut;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            oldMeshTop = mainSkinnMeshTop.sharedMesh;
            mainSkinnMeshTop.sharedMesh = newMeshTop;
            newMeshTop = oldMeshTop;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            oldMeshBottom = mainSkinnMeshBottom.sharedMesh;
            mainSkinnMeshBottom.sharedMesh = newMeshBottom;
            newMeshBottom = oldMeshBottom;
        }

    }
}
