using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ChangeModel : MonoBehaviour
{
    GameObject femaleModel1;
    GameObject femaleModel2;
    GameObject maleModel1;

    List<GameObject> gameObjectArray;
    List<Vector3> positionVectors;

    float timeElapsed = 3;
    float lerpDuration = 3;

    // Start is called before the first frame update
    void Start()
    {
        gameObjectArray = new List<GameObject>();
        positionVectors = new List<Vector3>();

        maleModel1 = GameObject.Find("Male_03_V02").gameObject;
        gameObjectArray.Add(maleModel1);

        femaleModel1 = GameObject.Find("Female_06_V01").gameObject;
        gameObjectArray.Add(femaleModel1);

        femaleModel2 = GameObject.Find("Female_05_V02").gameObject;
        gameObjectArray.Add(femaleModel2);

        Vector3 oneNextPostion = maleModel1.transform.position;
        Vector3 twoNextPosition = femaleModel1.transform.position;
        Vector3 threeNextPostion = femaleModel2.transform.position;

        positionVectors.Add(oneNextPostion);
        positionVectors.Add(twoNextPosition);
        positionVectors.Add(threeNextPostion);
    }

    // Update is called once per frame
    void Update()
    {
        if (timeElapsed < lerpDuration)
        {
            for (int counter = 0; counter < gameObjectArray.Count; counter++)
            {
                GameObject toChangePosition = gameObjectArray[counter];
                int next = counter + 1 > gameObjectArray.Count - 1 ? 0 : counter + 1;

                toChangePosition.transform.position = Vector3.Lerp(positionVectors[counter], positionVectors[next], timeElapsed / lerpDuration);
                timeElapsed += Time.deltaTime;
            }
        }
    }

    public void OnModelClick()
    {
        timeElapsed = 0.0f;
        positionVectors.Clear();

        foreach (GameObject gameObjectFromArray in gameObjectArray)
            positionVectors.Add(gameObjectFromArray.transform.position);
    }

}
