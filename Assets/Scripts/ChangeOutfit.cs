using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SharedNameSpace
{
    public class Manager
    {
        public static List<GameObject> allGameObjects = new List<GameObject>();
    }
}
public class ChangeOutfit : MonoBehaviour
{
    GameObject friendObj;
    float timerToRandomize = 0;

    SkinnedMeshRenderer mainSkinnMeshHairCut;
    SkinnedMeshRenderer otherSkinnMeshHairCut;

    Mesh newMeshHairCut;
    Mesh oldMeshHairCut;

    public string part = "";
    int randomObject = 1;

    // Start is called before the first frame update
    void Start()
    {
        friendObj = GameObject.Find("Female_02_V02").gameObject;
        SharedNameSpace.Manager.allGameObjects.Add(GameObject.Find("Male_03_V02").gameObject);
        SharedNameSpace.Manager.allGameObjects.Add(GameObject.Find("Female_05_V02").gameObject);
        SharedNameSpace.Manager.allGameObjects.Add(GameObject.Find("Female_06_V01").gameObject);


        if (!friendObj)
            Debug.Log("friednObject is null");

        Debug.Log(part);
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void RandomizeFriendObject()
    {

        switch (randomObject)
        {
            case 1:
            {

                friendObj = GameObject.Find("Male_04_V02").gameObject;
                randomObject += 1;
            }
            break;

            case 2:
            {
                friendObj = GameObject.Find("Female_02_V02").gameObject;
                randomObject += 1;

            }
            break;

            case 3:
            {
                friendObj = GameObject.Find("Male_04_V01").gameObject;
                randomObject += 1;
            }
            break;

            case 4:
            {
                friendObj = GameObject.Find("Male_01_V02").gameObject;
                randomObject = 1;
            }
            break;

            //case 7:
            //{
            //    friendObj = GameObject.Find("Female_01_V02").gameObject;
            //    randomObject += 1;
            //}
            //break;

            //case 8:
            //{
            //    friendObj = GameObject.Find("Female_01_V01").gameObject;
            //    randomObject += 1;
            //}
            //break;

            //case 9:
            //{
            //    friendObj = GameObject.Find("Female_03_V02").gameObject;
            //    randomObject += 1;
            //}
            //break;

            //case 10:
            //{
            //    friendObj = GameObject.Find("Male_01_V02").gameObject;
            //    randomObject = 1;
            //}
            //break;

        }

    }


    public void OnButtonClick()
    {
        foreach (GameObject model in SharedNameSpace.Manager.allGameObjects)
        {
            mainSkinnMeshHairCut = model.transform.Find(part).GetComponent<SkinnedMeshRenderer>();
            otherSkinnMeshHairCut = friendObj.transform.Find(part).gameObject.GetComponent<SkinnedMeshRenderer>();
            newMeshHairCut = otherSkinnMeshHairCut.sharedMesh;

            oldMeshHairCut = mainSkinnMeshHairCut.sharedMesh;
            mainSkinnMeshHairCut.sharedMesh = newMeshHairCut;
            newMeshHairCut = oldMeshHairCut;
        }
        RandomizeFriendObject();
    }
}
