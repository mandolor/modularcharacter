using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    float rotationSpeed = -60.0f;
    bool autoRotation = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      if(!autoRotation)
      {
          if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);

          if (Input.GetKey(KeyCode.D))
            transform.Rotate(-Vector3.up * rotationSpeed * Time.deltaTime);
      }

      if (Input.GetKeyDown(KeyCode.W))
          autoRotation = !autoRotation;

      if (Input.GetKeyDown(KeyCode.E))
          rotationSpeed *= -1;

      if(autoRotation)
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }
}
